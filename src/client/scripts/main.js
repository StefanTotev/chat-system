$(document).ready(function () {
    const socket = io();
    let ss = window.sessionStorage;
    let $window = $(window);
    let $messages = $('#messages');
    let $messagesPanel = $('.fixed-panel')[0];
    let $userList = $('#user-list');
    let $input = $('#message');
    let $btnSend = $('#send');
    let $mods = $('button.mods');

    /**
     * Listening for new connections
     */
    socket.on('connect', () => {
        if ( !ss.nickname ) {
            let nickname = prompt('Set a nickname...');
            socket.emit('newParticipant', nickname);
        } else {
            socket.emit('newParticipant', ss.nickname);
        }
    });

    /**
     * Listening whether the nickname is already used or not
     */
    socket.on('checkNickname', (data) => {
        if ( data.isUsed ) {
            let nickname = prompt('Set a nickname...');
            socket.emit('newParticipant', nickname);
        } else {
            ss.nickname = data.nickname;
            ss.time = new Date().getTime();
        }
    });

    /**
     * Listening for system messages
     */
    socket.on('systemMessage', (message) => {
        sendMessage(message);
    });

    /**
     * Listening for any changes in member list
     */
    socket.on('updateMemberList', (nickname) => updateClientList(nickname));

    /**
     * Declare an interval which holds the text on typing
     * @type {number}
     */
    let typingInterval = 0;

    /**
     * Listening if someone else is typing right now
     */
    socket.on('typingMessage', (nickname) => {
        clearInterval(typingInterval);
        $('.typing').fadeIn(300);
        $('.typing').html(nickname + " is typing...");

        typingInterval = setInterval(() => $('.typing').hide(), 600);
    });

    /**
     * Receiving new messages
     */
    socket.on('newMessage', (message) => {
        sendMessage(message);
    });

    /**
     * Creating a list button
     * TODO: Must be exported as helper function
     * @param nickname
     * @returns {Element}
     */
    function createMemberElement(nickname) {
        let button = document.createElement('button');
        let textNode = document.createTextNode(nickname);
        button.appendChild(textNode);
        button.setAttribute('type', 'button');
        button.setAttribute('class', 'list-group-item list-group-item-success no-border-radius');

        return button;
    }

    /**
     * When invoked refreshes the member list
     * @param data
     */
    function updateClientList(data) {
        $userList.empty();
        data.forEach(function (nick, index) {
            $userList.append(createMemberElement(nick));
        });
    }

    function sendMessage(message) {
        let isScrolledToBottom = $messagesPanel.scrollHeight - $messagesPanel.clientHeight <= $messagesPanel.scrollTop + 100;

        $messages.append("<li>" + message + "</li>");

        if (isScrolledToBottom) {
            $messagesPanel.scrollTop = $messagesPanel.scrollHeight - $messagesPanel.clientHeight + 100;
        }
    }

    function clearInput() {
        $input.val('');
    }

    let bold = false;
    let italic = false;
    let underscore = false;

    /** Check if some of the modifiers are used: BOLD, ITALIC, UNDERSCORE */
    $mods.on('click', function (e) {

        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
        }
        else {
            $(this).addClass('active');
        }

        switch ($(this).context.innerText.toLowerCase()) {
            case 'b':
                bold = !bold;
                break;
            case 'i':
                italic = !italic;
                break;
            case 'u':
                underscore = !underscore;
                break;
        }
    });

    $window.keydown(function (event) {
        let emitEvent = true;

        if (event.ctrlKey || event.metaKey || event.altKey) {
            $input.focus();
            emitEvent = false;
        }

        if (event.which === 13) {
            emitEvent = false;
            if ($input.val().length) {
                socket.emit('sendMessage', ss.nickname, $input.val());
                clearInput();
            }

            if (bold) $('.msgs').addClass('bolded');
            if (italic) $('.msgs').addClass('italic');
            if (underscore) $('.msgs').addClass('underscored');
        }

        if( emitEvent ) {
            socket.emit('typing', ss.nickname);
        }
    });
});