const _ = require('lodash');
const moment = require('moment');
const Participant = require('./Participant');

function Chat(io) {
    this.io = io;
    this.clients = [];
    this.rooms = [];
}

Chat.prototype.listen = function () {

    this.io.on('connection', (socket) => {

        socket.on('newParticipant', (nickname) => {

            this.checkNickname(nickname, socket, (client) => {
                /**
                 * Creating new participant object, if everything is fine
                 */
                this.addParticipant(nickname, socket.id);

                /**
                 * Notifying other participants that someone joined the chat
                 * TODO: This should be changed with channels
                 */
                client.broadcast.emit('systemMessage', nickname + ' has joined the chat!');

                /**
                 * Updates the member list to all connected participants
                 */
                this.io.emit('updateMemberList', this.getMemberList());
            });
        });

        socket.on('typing', (nickname) => {
            socket.broadcast.emit('typingMessage', nickname);
        });

        socket.on('sendMessage', (nickname, message) => {
           this.io.emit('newMessage', nickname + ": " + message);
        });

        socket.on('disconnect', (reason) => {
            const participant = this.getParticipantObject(socket.id);

            socket.broadcast.emit('systemMessage', participant.getNickname() + ' has left the chat!');
            this.removeParticipant(socket.id);
            this.io.emit('updateMemberList', this.getMemberList());
        })
    });
};

/**
 * Creates new Participant object and adds it to the other online participants' list
 * @param {String} nickname - Participant's nickname
 * @param {String} socketId - socket.id
 */
Chat.prototype.addParticipant = function (nickname, socketId) {
    let participant = new Participant(nickname, socketId);
    this.clients.push(participant);
};

/**
 * Remove specific participant from member list
 * @param {String} socketId - socket.id
 */
Chat.prototype.removeParticipant = function (socketId) {
    _.remove(this.clients, (o) => o.getSocketId() === socketId);
};

/**
 * Use this when you need to use the whole object of specific participant.
 * Converts the current socket.id to Participant object
 * @param {String} socketId - socket.id
 * @returns {Participant|Object}
 */
Chat.prototype.getParticipantObject = function(socketId) {
    return _.find(this.clients, (o) => o.getSocketId() === socketId);
};

/**
 * Get all online members
 * @returns {Array}
 */
Chat.prototype.getMemberList = function () {
    return _.flatMap(this.clients, (member) => member.getNickname());
};

/**
 * Check the participant nickname. If the nickname is already in use we send event to the client
 * to be asked again for new nickname.
 * @param {String} nickname - Participant's nickname
 * @param {Object} socket - Socket given when connect event is handled
 * @param {Function} cb - Callback function invoked only when the nickname is not used
 */
Chat.prototype.checkNickname = function (nickname, socket, cb) {
    let findUserIndex = _.findIndex(this.clients, (o) => o.getNickname() === nickname);
    if ( findUserIndex > -1 ) {
        socket.emit('checkNickname', {isUsed: true, nickname: nickname});
    }
    else {
        socket.emit('checkNickname', {isUsed: false, nickname: nickname});
        cb(socket);
    }
};

module.exports = function(socket) {
    return new Chat(socket);
};
