const debug = require('debug')('participant');

function Participant(nickname, socketId) {
    this.nickname = nickname;
    this.socketId = socketId;

    this._isBanned = false;
    this._ipAddress = null;
    console.info('new participant| nickname: %s | socket #: %s', nickname, socketId);
}

Participant.prototype.setNickname = function(name) {
    this.nickname = name;
};

Participant.prototype.getNickname = function() {
    return this.nickname;
};

Participant.prototype.setSocketId = function (socket) {
    this.socketId = socket;
};

Participant.prototype.getSocketId = function() {
    return this.socketId;
};

Participant.prototype.ban = function() {
    this._isBanned = true;
};

Participant.prototype.unban = function() {
    this._isBanned = false;
}

Participant.prototype.isBanned = function() {
    return this._isBanned;
};

Participant.prototype.getIPAddress = function() {
    return this._ipAddress;
};

Participant.prototype.setIPAddress = function(address) {
    if ( typeof address !== 'string' ) {
        throw new Error('IP Address must be string!');
    }
    this._ipAddress = address;
};

module.exports = Participant;

