const dotenv = require('dotenv').config();
const express = require('express');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const errorHandler = require('errorhandler');
const morgan = require('morgan');
const path = require('path');
const chat = require('./src/server/Chat')(io);

http.listen(process.env.PORT || 3000);

if ( process.env.NODE_ENV === 'development' ) {
    app.use(errorHandler());
}

/**
 * Middleware for exposing static files through different path
 */
app.use('/assets', express.static(path.join(__dirname, '/public/dist')),  express.static(path.join(__dirname, '/public/vendor')));

/**
 * Middleware watching for http requests and log them into console
 * 'tiny' is built-in log pattern
 */
app.use(morgan('tiny'));

/**
 * Handling index page request
 */
app.get('/', (req, res) => res.sendFile(path.join(__dirname, '/public/index.html')));

/**
 * Handling all other requests and send Not Found response
 */
app.all('*', (req, res) => res.sendStatus(404));

/**
 * Running the chat to start listen for events
 */
chat.listen();
