const gulp = require('gulp');
const concat = require('gulp-concat');
const mainBowerFiles = require('main-bower-files');
const filter = require('gulp-filter');
const rev = require('gulp-rev');
const revCollector = require('gulp-rev-collector');
const sourceMap = require('gulp-sourcemaps');
const csso = require('gulp-csso');
const minify = require('gulp-minify');

gulp.task('bower', () =>
    gulp.src(mainBowerFiles())
        .pipe(filter('**/*.js'))
        .pipe(minify({
            noSource: true,
            ext: {
                min: '.min.js'
            }
        }))
        .pipe(gulp.dest('public/vendor'))
);

gulp.task('scripts', () =>
    gulp.src('src/client/scripts/*.js')
        .pipe(sourceMap.init())
        .pipe(concat({path: 'bundle.js', cwd: './'}))
        .pipe(minify({
            noSource: true,
            ext: {
                min: '.min.js'
            }
        })).on('error', (err) => console.log(err))
        .pipe(rev())
        .pipe(sourceMap.write('.'))
        .pipe(gulp.dest('public/dist/js'))
        .pipe(rev.manifest('scripts-manifest.json'))
        .pipe(gulp.dest('src/client/rev'))
);

gulp.task('styles', () =>
    gulp.src('src/client/styles/*.css')
        .pipe(sourceMap.init())
        .pipe(concat({path: 'bundle.css', cwd: './'}))
        .pipe(rev())
        .pipe(csso())
        .pipe(sourceMap.write('.'))
        .pipe(gulp.dest('public/dist/css'))
        .pipe(rev.manifest('styles-manifest.json'))
        .pipe(gulp.dest('src/client/rev'))
);

gulp.task('revision', ['scripts', 'styles'], () => {
    gulp.src(['src/client/rev/**/*.json', 'public/index.html'])
        .pipe(revCollector({
            replaceReved: true
        }))
        .pipe(gulp.dest('public'));
});

gulp.task('watch', () => {
    gulp.watch('src/client/styles/*.css', ['styles', 'revision']);
    gulp.watch('src/client/scripts/*.js', ['scripts', 'revision']);
});

gulp.task('default', ['bower', 'revision', 'watch']);

/** TODO: Add minifying and revision to styles */